import os
import cv2
import time
import psutil
import datetime
import picamera  

def get_oldest_out(path):
    list_of_files = os.listdir(path)    
    full_path = ["{}{}".format(path, x) for x in list_of_files]
    for file_path in full_path:
        if os.path.isdir(file_path):
            full_path.remove(file_path)
    oldest_file = min(full_path, key=os.path.getctime)
    os.remove(oldest_file)

# initialize the camera and grab a reference to the raw camera capture
camera = picamera.PiCamera()
stream = picamera.PiCameraCircularIO(camera,seconds=5)
# allow the camera to warmup, then initialize the average frame, last
# uploaded timestamp, and frame motion counter
print("[INFO] warming up...")
time.sleep(20)
filename = datetime.datetime.now()
path = "/media/pi/CONFETTI/"
bytes_avail = psutil.disk_usage(path).free
gigabytes_avail = bytes_avail /1024/1024/1024
print(gigabytes_avail)
if gigabytes_avail < 1:
    get_oldest_out(path)
camera.start_recording('{}{}-{}-{} {} hour.h264'.format(path,
                                                        filename.year,
                                                        filename.month,
                                                        filename.day,
                                                        filename.hour))
while True:
    bytes_avail = psutil.disk_usage(path).free
    gigabytes_avail = bytes_avail /1024/1024/1024
    print(gigabytes_avail)
    if gigabytes_avail < 1:
        get_oldest_out(path)
    filename = datetime.datetime.now()
    if filename.hour < 9 or filename.hour > 20:
        pass
    camera.split_recording('{}{}-{}-{} {} hour{}.h264'.format(path,
                                                              filename.year,
                                                              filename.month,
                                                              filename.day,
                                                              filename.hour,
                                                              filename.second))
    camera.wait_recording(3600)

